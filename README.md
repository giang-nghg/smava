# smava_assignments

Dear applicant,

We've prepared 2 main assignments for you, please take your time to forfill them.

In the first assignment we ask you to show your understanding of OOP. A more detailed explanation can be found in the assignment itself

In the second assignment we'll ask you to show your data wrangling skills. Don't worry if you get a little confused, we'll definitely be able to figure out if you've got what it takes!

If you're done, please commit your changes to a new branch and do a pull request. 

Kind regards,

Your Smava Marketing Technology team
