from hypothesis import given
from hypothesis.strategies import just, one_of, nothing, none, booleans, integers, floats, text, iterables, sampled_from, from_regex

from pytest import raises

import math

from solution import OperationGenerator, XSI_TYPE_OF_KEYWORD_TYPE


class TestBidModifierAddingOperationGenerator:
    @given(
        bid_modifier=one_of(integers(), floats()),
        criterion_id=integers(min_value=1),
        ad_group_id=integers(min_value=1)
    )
    def test_output_format(self, bid_modifier, criterion_id, ad_group_id):
        operation = OperationGenerator.add_bid_modifier(bid_modifier, criterion_id, ad_group_id)

        assert operation['operator'] == 'ADD'
        assert operation['operand']['adGroupId'] == ad_group_id
        assert operation['operand']['criterion']['xsi_type'] == 'Platform'
        assert operation['operand']['criterion']['id'] == criterion_id
        assert math.isnan(operation['operand']['bidModifier']) and math.isnan(bid_modifier) or\
                operation['operand']['bidModifier'] == bid_modifier

    @given(bid_modifier=one_of(nothing(), none(), text(), iterables(integers())))
    def test_bid_modifier_not_numeric_type(self, bid_modifier):
        with raises(TypeError) as excinfo:
            OperationGenerator.add_bid_modifier(bid_modifier, 1, 1)
        assert 'bid_modifier must be a real number' in str(excinfo.value)

    @given(
        criterion_id=one_of(nothing(), none(), floats(), text(), iterables(integers()))
    )
    def test_criterion_id_not_integer_type(self, criterion_id):
        with raises(TypeError) as excinfo:
            OperationGenerator.add_bid_modifier(1, criterion_id, 1)
        assert 'criterion_id must be an integer' in str(excinfo.value)

    @given(
        ad_group_id=one_of(nothing(), none(), floats(), text(), iterables(integers()))
    )
    def test_ad_group_id_not_integer_type(self, ad_group_id):
        with raises(TypeError) as excinfo:
            OperationGenerator.add_bid_modifier(1, 1, ad_group_id)
        assert 'ad_group_id must be an integer' in str(excinfo.value)

    @given(
        criterion_id=one_of(integers(max_value=0), just(False))
    )
    def test_criterion_id_non_positive(self, criterion_id):
        with raises(ValueError) as excinfo:
            OperationGenerator.add_bid_modifier(1, criterion_id, 1)
        assert 'criterion_id must be positive' in str(excinfo.value)

    @given(
        ad_group_id=one_of(integers(max_value=0), just(False))
    )
    def test_ad_group_id_non_positive(self, ad_group_id):
        with raises(ValueError) as excinfo:
            OperationGenerator.add_bid_modifier(1, 1, ad_group_id)
        assert 'ad_group_id must be positive' in str(excinfo.value)


class TestKeywordAddingOperationGenerator:
    @given(
        text=text(),
        match_type=sampled_from(['BROAD', 'EXACT']),
        keyword_type=sampled_from(['biddable', 'negative']),
        ad_group_id=integers(min_value=1)
    )
    def test_output_format(self, text, match_type, keyword_type, ad_group_id):
        operation = OperationGenerator.add_keyword(text, match_type, keyword_type, ad_group_id)

        assert operation['operator'] == 'ADD'
        assert operation['operand']['xsi_type'] == XSI_TYPE_OF_KEYWORD_TYPE[keyword_type]
        assert operation['operand']['adGroupId'] == ad_group_id
        assert operation['operand']['criterion']['xsi_type'] == 'Keyword'
        assert operation['operand']['criterion']['matchType'] == match_type
        assert operation['operand']['criterion']['text'] == text

    @given(
        text=one_of(nothing(), none(), booleans(), integers(), floats(), iterables(integers()))
    )
    def test_text_is_not_string(self, text):
        with raises(TypeError) as excinfo:
            OperationGenerator.add_keyword(text, 'BROAD', 'biddable', 1)
        assert 'text must be a string' in str(excinfo.value)

    @given(
        match_type=from_regex(r'\b(?!BROAD|EXACT)\b\S+')  # Match all except these words
    )
    def test_invalid_match_type(self, match_type):
        with raises(ValueError) as excinfo:
            OperationGenerator.add_keyword('keyword', match_type, 'biddable', 1)
        assert 'match_type must be either BROAD or EXACT' in str(excinfo.value)

    @given(
        keyword_type=from_regex(r'\b(?!biddable|negative)\b\S+')
    )
    def test_invalid_keyword_type(self, keyword_type):
        with raises(KeyError):
            OperationGenerator.add_keyword('keyword', 'BROAD', keyword_type, 1)

    @given(
        ad_group_id=one_of(nothing(), none(), floats(), text(), iterables(integers()))
    )
    def test_ad_group_id_not_integer_type(self, ad_group_id):
        with raises(TypeError) as excinfo:
            OperationGenerator.add_keyword('keyword', 'BROAD', 'biddable', ad_group_id)
        assert 'ad_group_id must be an integer' in str(excinfo.value)

    @given(
        ad_group_id=one_of(integers(max_value=0), just(False))
    )
    def test_ad_group_id_non_positive(self, ad_group_id):
        with raises(ValueError) as excinfo:
            OperationGenerator.add_keyword('keyword', 'BROAD', 'biddable', ad_group_id)
        assert 'ad_group_id must be positive' in str(excinfo.value)


class TestCampaignStatusControlOperationGenerator:
    @given(
        status=sampled_from(['PAUSED', 'REMOVED']),
        campaign_id=integers(min_value=1)
    )
    def test_output_format(self, status, campaign_id):
        operation = OperationGenerator.set_campaign_status(status, campaign_id)

        assert operation['operator'] == 'SET'
        assert operation['operand']['id'] == campaign_id
        assert operation['operand']['status'] == status

    @given(
        status=from_regex(r'\b(?!PAUSED|REMOVED)\b\S+')
    )
    def test_invalid_status(self, status):
        with raises(ValueError) as excinfo:
            OperationGenerator.set_campaign_status(status, 1)
        assert 'status must be either PAUSED or REMOVED' in str(excinfo.value)

    @given(
        campaign_id=one_of(nothing(), none(), floats(), text(), iterables(integers()))
    )
    def test_ad_group_id_not_integer_type(self, campaign_id):
        with raises(TypeError) as excinfo:
            OperationGenerator.set_campaign_status('PAUSED', campaign_id)
        assert 'campaign_id must be an integer' in str(excinfo.value)

    @given(
        campaign_id=one_of(integers(max_value=0), just(False))
    )
    def test_campaign_id_non_positive(self, campaign_id):
        with raises(ValueError) as excinfo:
            OperationGenerator.set_campaign_status('PAUSED', campaign_id)
        assert 'campaign_id must be positive' in str(excinfo.value)