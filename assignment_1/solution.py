from numbers import Real, Integral


XSI_TYPE_OF_KEYWORD_TYPE = {
    'biddable': 'BiddableAdGroupCriterion',
    'negative': 'NegativeAdGroupCriterion'
}


class OperationGenerator:
    @staticmethod
    def add_bid_modifier(bid_modifier, criterion_id, ad_group_id):
        # Check data types and value ranges to avoid wasting erroneous API calls
        if not isinstance(bid_modifier, Real):
            raise TypeError('bid_modifier must be a real number')
        if not isinstance(criterion_id, Integral):
            raise TypeError('criterion_id must be an integer')
        if not isinstance(ad_group_id, Integral):
            raise TypeError('ad_group_id must be an integer')
        if criterion_id < 1:
            raise ValueError('criterion_id must be positive')
        if ad_group_id < 1:
            raise ValueError('ad_group_id must be positive')

        return {
            'operator': 'ADD',
            'operand': {
                'adGroupId': ad_group_id,
                'criterion': {
                    'xsi_type': 'Platform',
                    'id': criterion_id
                },
                'bidModifier': bid_modifier
            }
        }

    @staticmethod
    def add_keyword(text, match_type, keyword_type, ad_group_id):
        if not isinstance(text, str):
            raise TypeError('text must be a string')
        if match_type not in ['BROAD', 'EXACT']:
            raise ValueError('match_type must be either BROAD or EXACT')
        if not isinstance(ad_group_id, Integral):
            raise TypeError('ad_group_id must be an integer')
        if ad_group_id < 1:
            raise ValueError('ad_group_id must be positive')

        return {
            'operator': 'ADD',
            'operand': {
                'xsi_type': XSI_TYPE_OF_KEYWORD_TYPE[keyword_type],
                'adGroupId': ad_group_id,
                'criterion': {
                    'xsi_type': 'Keyword',
                    'matchType': match_type,
                    'text': text
                }
            }
        }

    @staticmethod
    def set_campaign_status(status, campaign_id):
        if status not in ['PAUSED', 'REMOVED']:
            raise ValueError('status must be either PAUSED or REMOVED')
        if not isinstance(campaign_id, Integral):
            raise TypeError('campaign_id must be an integer')
        if campaign_id < 1:
            raise ValueError('campaign_id must be positive')

        return {
            'operator': 'SET',
            'operand': {
                'id': campaign_id,
                'status': status
            }
        }