"""
There are 2 data sets in this repository. One contains keywords performance data, another one contains ad group data.
You can join the data together on the AdGroupId. Once that is done you can start with the assignments.

Please make sure you show clearly how you completed each assignment and feel free to comment anywhere to elaborate on
what you we're doing.

"""
import pandas as pd
import re
import numpy as np
from datetime import date, datetime


def prepare_dataframe():
    adgroups = pd.read_csv('data_sets/adgroups.csv')
    keywords = pd.read_csv('data_sets/keywords.csv')

    return pd.merge(adgroups, keywords, on='AdGroupId')

# 1: Calculate the effective_bid for each row
# Tip: The effective bid is the CpcBid * device modifier (mobile, tablet, desktop) related to the Device of that row.
# The modifiers are currently formatted as strings, you need to convert them to a factor so you can use them to
#
# calculate the effective_bid
#
# Output the result into assignment1.csv

def assignment_1(df):
    ## Convert modifier strings to float numbers
    def percentage_to_float(x):
        is_negative = bool(re.match(r'-', x))
        value = re.search(r'\d+', x)
        if value:
            value = float(value.group(0)) / 100
            if is_negative:
                value = -value
        else:
            value = 1  # I'm assuming missing modifier means it will not modify CPC, hence 1
        return value

    df['desktop_modifier'] = df['AdGroupDesktopBidModifier'].apply(percentage_to_float)
    df['mobile_modifier'] = df['AdGroupMobileBidModifier'].apply(percentage_to_float)
    df['tablet_modifier'] = df['AdGroupTabletBidModifier'].apply(percentage_to_float)

    ## Convert device strings to factors
    df['device_level'] = df['Device'].astype('category')

    ## Determine the correct modifier to use
    def bid_modifier(x):
        modifier_column_of = {
            'Computers': 'desktop_modifier',
            'Mobile devices with full browsers': 'mobile_modifier',
            'Tablets with full browsers': 'tablet_modifier'
        }
    
        x_device_level = x['device_level']
        return x[modifier_column_of[x_device_level]]

    df['bid_modifier'] = df.apply(bid_modifier, axis=1)

    ## Calculate effective bids
    df['effective_bid'] = df['CpcBid'] * df['bid_modifier']

    ## Save result
    df[['AdGroupId', 'effective_bid']].to_csv('assignment1.csv')

    return df

# 2: Add calender week numbers to the data set and aggregate all data to calender_week_nr, KeywordText, KeywordMatchType, Device level. Sum
#    Cost, Impressions and Clicks
#
# Output the restult into assignment2.csv

def assignment_2(df):
    ## Calculate week numbers
    df['calendar_week_nr'] = df['Date'].apply(datetime.strptime, args=('%Y-%m-%d',)).apply(date.isocalendar).apply(lambda x: x[1])

    ## Aggregate
    assignment_2 = df[[
        'calendar_week_nr', 'KeywordText', 'KeywordMatchType', 'device_level',
        'Cost', 'Impressions', 'Clicks']].groupby(['calendar_week_nr', 'KeywordText', 'KeywordMatchType', 'device_level']).sum()

    ## Save result
    assignment_2.to_csv('assignment2.csv')

    return df

# 3: Take the dataset you created in assignment 1. Find the cost of Keyword: kreditvergleich, KeywordMatchType: Exact,
#    Device: Computers and all rows available from february

def assignment_3(df):
    ## Extract months from dates
    df['month'] = df['Date'].apply(datetime.strptime, args=('%Y-%m-%d',)).apply(lambda date: date.month)

    ## Filter
    assignment_3 = df[
        (df['KeywordText'] == 'kreditvergleich') &\
        (df['KeywordMatchType'] == 'Exact') &\
        (df['device_level'] == 'Computers') &\
        (df['month'].astype('int') == 2)][['KeywordText', 'KeywordMatchType', 'device_level', 'month', 'Cost']]

    ## Save result
    assignment_3.to_csv('assignment3.csv')

    return df


if __name__ == '__main__':
    df = prepare_dataframe()
    df = assignment_1(df)
    df = assignment_2(df)
    df = assignment_3(df)